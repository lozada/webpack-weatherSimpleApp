const path = require('path')
const miniCssExtractPlugin = require('mini-css-extract-plugin')
const config = {
    mode: 'development',
    watchOptions: {
        poll: true
    },
    entry:{
        index: path.resolve(__dirname,'./src/js/index.js'),
    },
    output :{
        filename: 'js/[name].js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
          { test: /\.js$/,exclude: /(node_modules)/,use:{loader: 'babel-loader'}},
          {test: /\.styl$/, use: [miniCssExtractPlugin.loader, 'css-loader', 'stylus-loader'] },
          {test: /\.(jpg|jpeg|png|gif)$/, use:{
              loader: 'url-loader',
              options: {
                  limit: 2000000,
                  name: '[name].[ext]',
                  outputPath: 'assets/images/',
              }
          }}
     ]
    },
    plugins: [
        new miniCssExtractPlugin({
            filename: "css/[name].css"
        })
    ]
}
module.exports = config