class weather{
    constructor(keys = {}, q = ''){
        this.aaKey = keys.aaKey
        this.paKey = keys.paKey
        this.q = q
    }
    async getIpData(){
        try{
            const data = await fetch('http://ip-api.com/json')
            const dataJson = await data.json()
            const ip = await dataJson.query
            return ip
        }
        catch(e){
            return new Error(e)
        }
    }
    async getWeatherData(){
        try{
            const queryIP = await this.getIpData()
            const aaURL = await `https://api.apixu.com/v1/current.json?key=${this.aaKey}&q=${this.q.length == 0 ? queryIP : this.q  }`
            const weatherData = await fetch(aaURL)
            const weatherDataJson = await weatherData.json()
            return weatherDataJson
        }
        catch(e){
            return new Error(e)
        }
    }
    async getImage(q){
        try{
            const query = await `https://pixabay.com/api/?key=${this.paKey}&q=${q}&orientation=horizontal&pretty=false&per_page=3&min_width=1280&min_height=720`
            const get = await fetch(query)
            const images = await get.json()
            const image = await images.hits 
            const imageURL = await image[0].webformatURL
            return imageURL
        }
        catch(e){
            return 'hubo-un-error'
        }        
    }
    async getCompleteData(){
        try{
            const wData = await this.getWeatherData()
            const wDataName = wData.location.name
            const wDataCountry = wData.location.country
            const iuData = `${wDataName} ${wDataCountry}`
            const iData = await this.getImage(iuData)
            const result = []
            await result.push(wData,iData)
            return result
        }
        catch(e){
            return new Error(e)
        }
    }
}
export default weather