const createCard = (data) =>{
    const city = data[0].location.name
    const country = data[0].location.country
    const temp = data[0].current.temp_c
    const condition = data[0].current.condition.text
    const conditionIcon = data[0].current.condition.icon
    const image = data[1]
    
    const CE = (e) => document.createElement(e)
    
    const container = document.getElementById('weather-cards')

    const card = CE('div')
    card.setAttribute('class','card')

    const cardData = CE('div')
    cardData.setAttribute('class', 'card-data')

    const data1 = CE('p')
    data1.textContent = `${city}, ${country} ${temp}`

    const data2 = CE('p')
    data2.textContent = `${condition} `

    const data2Icon = CE('img')
    data2Icon.setAttribute('src',conditionIcon)

    data2.appendChild(data2Icon)
    cardData.appendChild(data1)
    cardData.appendChild(data2)
    card.appendChild(cardData)
    card.style.backgroundImage = `url('${image}')`
    container.append(card)
}

export default createCard