import weather from './weather-class'
import createCard from './cards'
import '../css/style.styl'
import { create } from 'domain';
const config = {
    aaKey: 'fc7a1adba7a14db3984234740182006',
    paKey: '9347753-180ede649bf3b9db98c331cd4'
}

const onSubmitData = () =>{
    const form = document.getElementById('weather-form')
    form.addEventListener('submit',async (e) => {

        e.preventDefault()
        const dataInput = document.getElementById('dataQuery').value
        const card = new weather(config, dataInput)
        const result = await card.getCompleteData()
        const dataArr = result
        createCard(dataArr)
    })
}



(async () => {
    const uInit = new weather(config)
    const result = await uInit.getCompleteData()
    const dataArr = result
    createCard(dataArr)
    onSubmitData()
})()

